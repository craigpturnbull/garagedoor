#include <SPI.h>
#include <Ethernet.h>

byte mac[] = {0x90,0xA2,0xDA,0x0D,0x48,0xD3};

EthernetServer server(80);
String readString;

#define Door_UP_PIN  3  //rly4
#define Door_DN_PIN  2 //rly3
#define Spare_1      4 //rly2
#define Spare_2      5 //rly1


#define Button_OPEN  8
#define Button_CLOSE  7
#define LightBeam 6

#define RelayON  HIGH 
#define RelayOFF  LOW

#define SWDebounceLimit 10000

int iDoorCounter = 0;
int iDoorCounterLimit = 24;
bool bTimerEnabled = false;
bool bDoorClosing = false;
bool bDoorOpening = false;

int SWOpenCount;
int SWCloseCount;

bool buttonOpen;
bool buttonClose;

void setup() {
  // put your setup code here, to run once:

  digitalWrite(Door_UP_PIN,RelayOFF);
  digitalWrite(Door_DN_PIN,RelayOFF);
  digitalWrite(Spare_1,RelayOFF);
  digitalWrite(Spare_2,RelayOFF);
  
  pinMode(Door_UP_PIN,OUTPUT);
  pinMode(Door_DN_PIN,OUTPUT);
  pinMode(Spare_1,OUTPUT);
  pinMode(Spare_2,OUTPUT);
  

  pinMode(Button_OPEN,INPUT);
  pinMode(LightBeam,INPUT);

  TCCR1A = 0; // set entire TCCR2A register to 0
  TCCR1B = 0; // set entire TCCR2B register to 0
  TCNT1  = 0; //initialize counter value to 0
  OCR1A = 15624; // = (16*10^6) / (1*1024) - 1 (16Mhz / 1024)
    // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  Serial.begin(9600);
  //OpenDoor();

  
Serial.println("Initialise Ethernet with DHCP");
   if(Ethernet.begin(mac) == 0)
   {
      Serial.println("Failed to configure Ethernet using DHCP");
      if (Ethernet.hardwareStatus() == EthernetNoHardware)
      {
        Serial.println("Ethernet shiled not found");
      }
      else if (Ethernet.linkStatus() == LinkOFF)
      {
        Serial.println("Ethernet cable is not connected");
      }
      while(true){delay(1);} // do nothing for ever
   }

    Serial.print("My IP address is");
    Serial.println(Ethernet.localIP());


  
}

void loop() 
{
  // put your main code here, to run repeatedly:
  
  //read the pins if

  
buttonOpen = digitalRead(Button_OPEN) == HIGH;
buttonClose = digitalRead(Button_CLOSE) == HIGH;

  
  if (buttonOpen && !buttonClose)
  {
    
    
    SWOpenCount++;
    if (SWOpenCount > SWDebounceLimit)
    {
      SWOpenCount = 0;
      Serial.println("Switch Requested Open Door");
      OpenDoor("button");
    }
  }

  if (buttonClose && !buttonOpen)
  {
    SWCloseCount++;
    if (SWCloseCount > SWDebounceLimit)
    {
      SWCloseCount = 0;
      Serial.println("Switch Requested Close Door");
      CloseDoor("button");
    }
  }

if (buttonClose && buttonOpen)
{
  TimerDone();
}
  
  if(digitalRead(LightBeam) == HIGH && bDoorClosing)
  {
    
    Serial.println("Light Beam Requested Open Door");
    OpenDoor("beam");
  }
  
  //Serial.println("Waiting for client");
  
  EthernetClient client = server.available();
  if(client)
  {
    Serial.println("new client");
    while(client.connected())
    {
      if (client.available())
      {
        char c = client.read();

        if(readString.length() < 100)
        {
          readString += c;
          //Serial.write(c);
          if (c == '\n')
          {
            Serial.println(readString);

            client.println("HTTP/1.1 200 OK"); //send new page
            client.println("Content-Type: text/html");
            client.println("Connection: Close");
            client.println();
            client.println("<HTML>");
            client.println("<HEAD>");
            client.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("</HEAD>");
            client.println("<BODY>");
            client.println("6 Border Way Garage Door Control<br />");
            client.println("<br />");
            client.println("<a href=\"/?opendoor\"\">Open</a>");
            client.println("<br />");
            client.println("<br />");
            client.println("<a href=\"/?closedoor\"\">Close</a><br />");    
            client.println("<br />");
            client.println("<br />");
            client.println("<a href=\"/?stopdoor\"\">Stop</a><br />");   
            
            client.println("</BODY>");
            
            client.println("</HTML>");


            //send the page
            delay(1);
            client.stop();

  //-------------------------------------------------
            // Code which needs to be Implemented:
            if(readString.indexOf("?opendoor") >0)//checks for on
            {
              OpenDoor("web");
            }
            if(readString.indexOf("?closedoor") >0)//checks for off
            {
                CloseDoor("web");
            }

            if(readString.indexOf("?stopdoor") >0)//checks for stop
            {
                TimerDone();
            }
            
            //clearing string for next read
            readString="";

            // give the web browser time to receive the data
            delay(1);
            // close the connection:
            client.stop();
            Serial.println("client disonnected");
            
          }
        }     
       }    
      }   
    }
}

void OpenDoor(String requestedBy)
{
  if(bDoorClosing) TimerDone();
  else{
  bDoorClosing = false;
  bDoorOpening = true;
  Serial.println("Opening Door - " + requestedBy);
  digitalWrite(Door_DN_PIN,RelayOFF);
  digitalWrite(Door_UP_PIN,RelayON);
  delay(1000);
  StartTimer();
  }
}


void CloseDoor(String requestedBy)
{
  if(bDoorOpening) TimerDone();
  else{
  bDoorClosing = true;
  bDoorOpening = false;
  Serial.println("Closing Door - " + requestedBy);
  digitalWrite(Door_UP_PIN,RelayOFF);
  digitalWrite(Door_DN_PIN,RelayON);
  delay(1000);
  StartTimer();
  }
}

void TimerDone()
{
  bDoorClosing = false;
  bDoorOpening = false;
  Serial.println("Timer Done");
  bTimerEnabled = false; 
  digitalWrite(Door_UP_PIN,RelayOFF);
  digitalWrite(Door_DN_PIN,RelayOFF);
  delay(1000);
}

void StartTimer()
{
  Serial.println("Timer Starting");
  iDoorCounter = 0;
  bTimerEnabled = true; 
}


ISR(TIMER1_COMPA_vect)
{ 
  if (bTimerEnabled)
  {
    if(iDoorCounter < iDoorCounterLimit)
    {
      iDoorCounter++;
    }
    else
    { 
      TimerDone();    
    }
  }
}
